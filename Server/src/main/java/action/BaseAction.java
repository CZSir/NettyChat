package action;

import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import service.ResponseService;

/**
 * 基本网络请求
 *
 * */
@Component
public abstract class BaseAction {

    @Autowired
    protected ResponseService responseService;

    protected Object deserializeBody(String body, Class clazz) {
        return JSON.parseObject(body, clazz);
    }

    protected String serializeJson(Object object) {
        return JSON.toJSONString(object);
    }
}
