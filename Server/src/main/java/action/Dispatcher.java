package action;

import io.netty.channel.Channel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import service.ResponseService;
import transport.protocol.MessageHolder;
import transport.protocol.ProtocolHeader;

@Component
public class Dispatcher {

    @Autowired
    ResponseService responseService;

    @Autowired
    LoginAction loginAction;

    public void dispatch(MessageHolder messageHolder) {
        if (messageHolder.getSign() != ProtocolHeader.REQUEST) {
            // 请求错误
            responseService.serverError(messageHolder.getChannel(), messageHolder.getSign());
            return;
        }

        String body = messageHolder.getBody();
        Channel channel = messageHolder.getChannel();
        switch (messageHolder.getType()) {
            // 登录
            case ProtocolHeader.LOGIN :
                loginAction.login(body, channel);
                break;

            // 登出
            case ProtocolHeader.LOGOUT :
                loginAction.logout(body, channel);
                break;


        }
    }

}
