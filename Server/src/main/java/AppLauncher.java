import io.netty.channel.socket.nio.NioServerSocketChannel;
import transport.netty.NettyConfig;
import transport.netty.NettyProcessor;
import transport.service.AbstractChatService;
import transport.service.SimpleChatService;

public class AppLauncher {

    public static void main(String[] args) {
        AbstractChatService chatService = new SimpleChatService();
        chatService.initAndStart();

        NettyConfig processor = new NettyProcessor();
        processor.setChannel(NioServerSocketChannel.class);
        processor.setParentGroup(1);
        processor.setChildGroup();
        processor.setHandler();
        processor.bind(12345);

        processor.start(true);
    }

}
