package queue;

import transport.protocol.MessageHolder;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class TaskQueue {

    private volatile static BlockingQueue<MessageHolder> messageQueue;

    public static BlockingQueue<MessageHolder> getQueue() {
        if (null == messageQueue) {
            synchronized (TaskQueue.class) {
                if (null == messageQueue) {
                    messageQueue = new LinkedBlockingQueue<MessageHolder>(1024);
                }
            }
        }

        return messageQueue;
    }

}
