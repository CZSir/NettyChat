package transport.service;

public abstract class AbstractChatService implements ChatService {

    public void initAndStart() {
        init();
        start();
    }
}
