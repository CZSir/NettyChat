package transport.service;

public interface ChatService {
    
    void init();
    
    void start();
    
}
