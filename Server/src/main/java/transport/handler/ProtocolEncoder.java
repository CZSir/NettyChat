package transport.handler;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import transport.protocol.MessageHolder;
import transport.protocol.ProtocolHeader;

public class ProtocolEncoder extends MessageToByteEncoder<MessageHolder> {

    protected void encode(ChannelHandlerContext ctx, MessageHolder msg, ByteBuf out)
            throws Exception {
        String body = msg.getBody();
        if (body == null) {
            throw new NullPointerException("body == null");
        }

        byte[] bytes = body.getBytes("utf-8");
        out.writeShort(ProtocolHeader.MAGIC)
                .writeByte(msg.getSign())
                .writeByte(msg.getType())
                .writeByte(msg.getStatus())
                .writeInt(bytes.length)
                .writeBytes(bytes);
    }
}
