package service;

import io.netty.channel.Channel;
import io.netty.util.concurrent.Future;
import org.springframework.stereotype.Service;
import transport.protocol.MessageHolder;
import transport.protocol.ProtocolHeader;

@Service
public class ResponseService {

    /**
     * 服务器忙响应
     *
     * */
    public void serverBusy(Channel channel, byte type) {
        if (null == channel)
            return;
        response(channel, ProtocolHeader.RESPONSE, type, ProtocolHeader.SERVER_BUSY);
    }

    /**
     * 服务器错误响应
     *
     * */
    public void serverError(Channel channel, byte type) {
        if (null == channel)
            return;
        response(channel, ProtocolHeader.RESPONSE, type, ProtocolHeader.SERVER_ERROR);
    }

    /**
     * 登录业务响应
     *
     * */
    public Future loginResponse(Channel channel, byte status, String body) {
        if (null == channel)
            return null;
        return response(channel, ProtocolHeader.RESPONSE, ProtocolHeader.LOGIN, status, body);
    }

    /**
     * 基本响应
     *
     * */
    private Future response(Channel channel, byte sign, byte type, byte status) {
        return response(channel, sign, type, status, "");
    }

    /**
     * 基本响应
     *
     * */
    private Future response(Channel channel, byte sign, byte type, byte status, String body) {
        MessageHolder messageHolder = new MessageHolder();
        messageHolder.setSign(sign);
        messageHolder.setType(type);
        messageHolder.setStatus(status);
        messageHolder.setBody(body);
        return channel.writeAndFlush(messageHolder);
    }
}
