package utils;

import com.alibaba.fastjson.JSON;

public class JsonUtils {

    public static String toJson(Object object) {
        return JSON.toJSONString(object);
    }

    public static Object parseObject(String json, Class clazz) {
        return JSON.parseObject(json, clazz);
    }
}
