package netty.conn;

public interface Connection {

    void connect(final String host, final int port);

}
