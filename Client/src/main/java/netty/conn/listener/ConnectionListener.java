package netty.conn.listener;


import io.netty.channel.Channel;

public interface ConnectionListener {

    void onConnectionComplete(Channel channel);

    void onConnectionError(Channel channel);
}
