package action;

import action.listener.ReceiverListener;

public abstract class BaseAction extends Client {

    public static ReceiverListener receiver;

    public void setReceiver(ReceiverListener receiver) {
        this.receiver = receiver;
    }
}
