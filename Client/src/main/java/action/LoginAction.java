package action;

import action.common.Const;
import action.listener.ActionListener;
import action.listener.ReceiverListener;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import netty.conn.AbstractConnection;
import netty.conn.CommonConnection;
import netty.conn.listener.ConnectionListener;
import netty.handler.MessageHolder;
import netty.handler.ProtocolHeader;
import org.apache.log4j.Logger;
import pojo.mo.Account;
import utils.JsonUtils;

public class LoginAction extends BaseAction implements ConnectionListener {

    private static Logger logger = Logger.getLogger(LoginAction.class);

    private String userName;

    private String password;

    private ActionListener actionListener;

//    public static ReceiverListener receiver;

    private AbstractConnection connection;

    public LoginAction() {
    }

    public LoginAction(ActionListener actionListener) {
        this.actionListener = actionListener;
    }

    public void dologin(String userName, String password) {
        isLogOut = true;

        this.userName = userName;
        this.password = password;

        this.connection = new CommonConnection();
        this.connection.setConnectionListener(this);

        this.connection.connect(Const.host, Const.port);
    }

    public void onConnectionComplete(Channel channel) {
        mChannel = channel;

        if (isLogOut) {
            Account account = new Account();
            account.setUsername(this.userName);
            account.setPassword(this.password);
            sendRequest(JsonUtils.toJson(account));
        }
    }

    public void onConnectionError(Channel channel) {
        isLogOut = true;
        mToken = null;

        logger.info("链接服务器异常（" + Const.host + ":" + Const.port + "）");
    }

    public void sendRequest(String body) {
        final MessageHolder messageHolder = new MessageHolder();
        messageHolder.setBody(body);
        messageHolder.setType(ProtocolHeader.LOGIN);
        messageHolder.setStatus((byte) 0);
        messageHolder.setSign(ProtocolHeader.REQUEST);
        ChannelFuture future = mChannel.writeAndFlush(messageHolder);
        future.addListener(new ChannelFutureListener() {
            public void operationComplete(ChannelFuture future) throws Exception {
                if (null == actionListener)
                    return;

                if (future.isSuccess()) {
                    actionListener.success(messageHolder);
                } else {
                    actionListener.failure(messageHolder);
                    mChannel.writeAndFlush(messageHolder);
                }
            }
        });
    }

    public static void main(String[] args) {
        LoginAction action = new LoginAction();

        action.setReceiver(new ReceiverListener() {
            public void receive(MessageHolder messageHolder) {
                logger.info("response : " + messageHolder);
            }
        });

        action.dologin("chen", "111");

    }
}
