package action.listener;

import netty.handler.MessageHolder;

public interface ActionListener {

    void success(MessageHolder messageHolder);

    void failure(MessageHolder messageHolder);
}
