package action.listener;

import netty.handler.MessageHolder;

public interface ReceiverListener {

    void receive(MessageHolder messageHolder);

}
