package action;

public interface Action {

    void sendRequest(String body, byte type);

}
