package action;

import action.listener.ReceiverListener;
import netty.handler.MessageHolder;
import netty.handler.ProtocolHeader;

public class Dispatcher {

    public static void dispatch(MessageHolder messageHolder) {
        switch (messageHolder.getType()) {
            case ProtocolHeader.LOGIN:
                ReceiverListener listener = LoginAction.receiver;
                listener.receive(messageHolder);
                break;
        }
    }
}
